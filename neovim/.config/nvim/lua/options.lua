local opt = vim.opt

-- line numbers
vim.wo.number = true
vim.wo.relativenumber = true

-- use spaces for tabs
opt.expandtab = true
opt.shiftround = true
opt.shiftwidth = 2
opt.tabstop = 2

-- show when leader key is pressed
opt.showcmd = true

-- highlight cursor line
opt.cursorline = true

opt.colorcolumn="80"

-- set clipboard
opt.clipboard = "unnamedplus"

-- search settings
opt.ignorecase = true
opt.smartcase = true -- assume we want case sensitive if uppercase
opt.inccommand = "split" -- preview of subsitution in a window
