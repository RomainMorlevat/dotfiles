return(
  {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    config = function ()
      local configs = require("nvim-treesitter.configs")

      configs.setup(
        {
          -- A list of parser names, or "all"
          ensure_installed = {
            "css",
            "html",
            "javascript",
            "lua",
            "markdown",
            "markdown_inline",
            "ruby",
            "vim"
          },

          -- Install parsers synchronously (only applied to `ensure_installed`)
          sync_install = false,
          auto_install = true,
          incremental_selection = {
            enable = true,
            keymaps = {
              init_selection = "<C-s>", -- set to `false` to disable one of the mappings
              node_incremental = "<C-s>",
              scope_incremental = false,
              node_decremental = "<bs>",
            },
          },
          indent = {
            enable = true,
          },
          highlight = {
            enable = true,
          }
        }
      )
    end
  }
)
