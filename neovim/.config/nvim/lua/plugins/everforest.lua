return {
	"neanias/everforest-nvim",
	name = "everforest",
	priority = 1000,
	config = function()
		require("everforest").setup({
			background = "soft",
			italics = true,
			show_end_of_buffer = true,
		})
		vim.cmd.colorscheme("everforest")
	end,
}
