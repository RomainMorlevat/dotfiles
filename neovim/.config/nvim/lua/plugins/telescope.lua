return {
	{
		"nvim-telescope/telescope.nvim",
		tag = "0.1.5",
		dependencies = { "nvim-lua/plenary.nvim" },
		config = function()
			local actions = require("telescope.actions")
			require("telescope").setup({
				defaults = {
					file_ignore_patterns = { "coverage", "node_modules" },
					layout_strategy = "flex",
				},
				pickers = {
					buffers = {
						mappings = {
							i = {
								["<C-d>"] = actions.delete_buffer + actions.move_to_top,
							},
						},
					},
				},
				-- telescope-ui-select
				extensions = {
					["ui-select"] = {
						require("telescope.themes").get_dropdown({}),
					},
				},
				-- end telescope-ui-select
			})

			local builtin = require("telescope.builtin")
			vim.keymap.set("n", "<c-p>", builtin.find_files, {})
			-- find through previously opened files
			vim.keymap.set("n", "<Space><Space>", builtin.oldfiles, {})
			-- show buffers
			vim.keymap.set("n", "<leader>b", builtin.buffers, {})
			-- live grep
			vim.keymap.set("n", "<leader>fg", builtin.live_grep, {})
			-- live grep current string in pwd
			vim.keymap.set("n", "<leader>lg", builtin.grep_string, {})
			-- navigate by symbols in current buffer
			vim.keymap.set("n", "<leader>ss", builtin.lsp_document_symbols, {})
			-- show quickfix list
			vim.keymap.set("n", "<leader>kf", builtin.quickfix, {})

			-- telescope-ui-select (to be called after setup)
			require("telescope").load_extension("ui-select")
			-- end telescope-ui-select
		end,
	},
	{
		"nvim-telescope/telescope-ui-select.nvim",
	},
}
