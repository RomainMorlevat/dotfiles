local map = vim.keymap.set

vim.g.mapleader = " "

map("n", "<leader>h", ":nohlsearch<CR>", { desc = "Clear search highlighting" })

-- move block up/down
map("v", "J", ":m '>+1<CR>gv=gv", { desc = "Move block up" })
map("v", "K", ":m '<-2<CR>gv=gv", { desc = "Move block down" })

map("n", "J", "mzJ`z", { desc = "cursor stay in place when joining lines" })

-- center the cursor when half page jumping
map("n", "<C-d>", "<C-d>zz", { desc = "center the cursor when jumping down" })
map("n", "<C-u>", "<C-u>zz", { desc = "center the cursor when jumping up" })

-- keep search term in the center
map("n", "n", "nzzzv", { desc = "center the searched term when moving forward" })
map("n", "N", "Nzzzv", { desc = "center the searched term when moving backward" })

-- multi search and edit
map(
	"n",
	"<leader>s",
	[[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]],
	{ desc = "search and edit word under the cursor" }
)

-- Move to previous/next buffer
map("n", "<A-h>", ":bp<CR>", { desc = "move to previous buffer" })
map("n", "<A-l>", ":bn<CR>", { desc = "move to next buffer" })

-- switch to last buffer
map("n", "<A-b>", ":b#<CR>", { desc = "Switch to last buffer"})

-- copy file path + line number
map(
	"n",
	"<leader>cp",
	":call setreg('+', expand('%:.') .. ':' .. line('.'))<CR>",
	{ desc = "copy file path with line number" }
)

map("ia", "bb", "byebug", { desc = "Abbr for Ruby debugger" })
