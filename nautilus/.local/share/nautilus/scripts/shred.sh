#!/bin/bash

# Count the number of selected files
IFS=$'\n'
selected_files=($NAUTILUS_SCRIPT_SELECTED_FILE_PATHS)
file_count=${#selected_files[@]}

# Create the confirmation message
if [ "$file_count" -eq 1 ]; then
  message="Are you sure you want to securely shred this file?"
else
  message="Are you sure you want to securely shred these $file_count files?"
fi

# Show confirmation dialog
zenity --question --text="$message" --width=300

# Check the user's response
if [ $? -eq 0 ]; then
  # User clicked "Yes"
  for file in "${selected_files[@]}"; do
    shred -n 2 -vzu "$file" | zenity --progress --pulsate --auto-close --text="Shredding: $file" --width=300
  done
  zenity --info --text="Shredding complete." --width=200
else
  # User clicked "No" or closed the dialog
  zenity --info --text="Operation cancelled." --width=200
fi
