HISTFILE=~/.zsh_history
HISTSIZE=1000
SAVEHIST=1000
setopt SHARE_HISTORY
setopt APPEND_HISTORY
# setopt INC_APPEND_HISTORY # not compatible with SHARE_HISTORY
setopt HIST_IGNORE_DUPS
setopt HIST_REDUCE_BLANKS
setopt HIST_SAVE_NO_DUPS

# Lines configured by zsh-newuser-install
unsetopt autocd extendedglob
# End of lines configured by zsh-newuser-install

# The following lines were added by compinstall
zstyle :compinstall filename '~/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
zstyle ':completion:*' menu select

# vi mode enhancement from oh-my-zsh
ZVM_INIT_MODE=sourcing
source ~/.config/zsh/zsh-vi-mode.plugin.zsh
# Following keybindings to move cursor with Ctrl+arrow
bindkey -M vicmd '^[[1;5C' emacs-forward-word
bindkey -M vicmd '^[[1;5D' emacs-backward-word
bindkey -M viins '^[[1;5C' emacs-forward-word
bindkey -M viins '^[[1;5D' emacs-backward-word
# Restore the arrow up/down completion
zvm_bindkey vicmd '^[[A' history-search-backward
zvm_bindkey viins '^[[A' history-search-backward
zvm_bindkey vicmd '^[[B' history-search-forward
zvm_bindkey viins '^[[B' history-search-forward

# Start ssh-agent with systemd user
if [ -z "${SSH_CONNECTION}" ] && ! [ -f "/run/.toolboxenv" ]; then
  export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
fi

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# to search in history with arrow keys
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
bindkey '^[OA' history-substring-search-up
bindkey '^[OB' history-substring-search-down

# must be sourced after $PATH and after sourced framework
export PATH="${ASDF_DATA_DIR:-$HOME/.asdf}/shims:$PATH"

export EDITOR='nvim'
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

source ~/.aliases
source ~/.env

# heroku autocomplete setup
HEROKU_AC_ZSH_SETUP_PATH=~/.cache/heroku/autocomplete/zsh_setup && test -f $HEROKU_AC_ZSH_SETUP_PATH && source $HEROKU_AC_ZSH_SETUP_PATH;

# fzf needs to be sourced after other autocompletions in order to avoid conflicts
source ~/.fzfrc
# use fzf for autocompletion
source ~/.config/zsh/plugins/fzf-tab/fzf-tab.plugin.zsh
# fzf-tab tmux plugin to have the menu in a popup
zstyle ':fzf-tab:*' fzf-command ftb-tmux-popup

export PATH="$HOME/.tmux/plugins/tmuxifier/bin:$PATH"
eval "$(tmuxifier init -)"

# needs to be sourced after autocompletion
eval "$(zoxide init zsh)"

if which starship &>/dev/null; then
  eval "$(starship init zsh)"
fi
