# dotfiles

After some years with a private git bare repository to manage my dotfiles, I've decided to start with a fresh config on a my new laptop.

## GNU Stow

I've chosen to use [GNU Stow](https://www.gnu.org/software/stow/) to give it a try.

### How to use

- Create a directory for the stow package named after the tool you want to stow ;
- move the config in that directory with the same file structure from ~/.

`stow -v -R -t ~ STOW_PACKAGE`

- ‘-R’: Restow (first unstow, then stow again) the package names that follow this option. This is useful for pruning obsolete symlinks from the target tree after updating the software in a package. This option may be repeated any number of times.

This will create symlinks in home directory following the tree structure found in STOW_PACKAGE.

## Git

Install [git-delta](https://github.com/dandavison/delta).

## Tmux

[TPM](https://github.com/tmux-plugins/tpm) must be installed and then the plugins listed in .tmux.conf.

## Zsh

Install [Zsh](https://www.zsh.org/) along `zsh-history-substring-search` and `zsh-syntax-highlighting`.

Prompt is [starship](https://github.com/starship/starship).

You also need `fzf` which needs `fd` and `ripgrep`.

[eza](https://github.com/eza-community/eza.git) is needed has a replacement to ls.
